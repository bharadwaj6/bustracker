from __future__ import unicode_literals

from django.apps import AppConfig


class BusLocationConfig(AppConfig):
    name = 'bus_location'
