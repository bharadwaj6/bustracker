import csv
from datetime import datetime

from django.core.management.base import BaseCommand
from django.contrib.gis.geos import *

from bus_location.models import BusLocation


class Command(BaseCommand):
    args = ''
    help = 'To copy csv data into database'

    def handle(self, *args, **options):
        with open('data_dumps/data_dump.csv', 'r') as dumpfile:
            csv_reader = csv.reader(dumpfile, delimiter=',')
            csv_reader.next()  # skip first line

            # the file can be really large, take some chunks at once and bulk update the database
            location_buffer = []
            for row in csv_reader:
                if len(location_buffer) < 500:
                    location_buffer.append(BusLocation(
                        bus_id=row[0],
                        gps_timestamp=datetime.strptime(row[1], "%m/%d/%Y %H:%M"),
                        location=Point(float(row[2]), float(row[3])),
                        speed=row[4]
                    ))
                else:
                    BusLocation.objects.bulk_create(location_buffer)
                    location_buffer = []
