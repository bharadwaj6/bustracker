from django.core.management.base import BaseCommand

from bus_location.models import BusLocation, HaltLocation


class Command(BaseCommand):
    args = ''
    help = 'To create halt locations database'

    def handle(self, *args, **options):
        prev_location = None
        prev_bus = None
        start_time = None
        end_time = None
        # import pdb
        # pdb.set_trace()
        locations_to_save = []
        for bus_location in BusLocation.objects.all():
            if not prev_location:
                prev_location = bus_location.location
                start_time = bus_location.gps_timestamp
                prev_bus = bus_location.bus_id
            else:
                if bus_location.location == prev_location and bus_location.bus_id == prev_bus:
                    end_time = bus_location.gps_timestamp
                else:
                    if bus_location.bus_id == prev_bus:
                        # location has changed for same bus
                        end_time = bus_location.gps_timestamp

                        # take time diff
                        time_diff = end_time - start_time
                        halt_location = prev_location

                        # store the time diff and halt location in halt
                        halt_time = int(time_diff.total_seconds())
                        if halt_time > 0:
                            # not sure why this is happening, but seems like there are halt times of 0 also
                            print "will store location: {} and duration: {} for bus: {}".format(
                                str(halt_location), str(halt_time), str(prev_bus)
                            )
                            locations_to_save.append(
                                HaltLocation(location=halt_location, duration=halt_time, bus_id=prev_bus)
                            )

                        # set current location as previous location and set new start time and end time
                        prev_location = bus_location.location
                        start_time = bus_location.gps_timestamp
                        end_time = None
                    else:
                        # if bus has changed
                        prev_bus = bus_location.bus_id
                        prev_location = bus_location.location
                        start_time = bus_location.gps_timestamp

        # save all the halt locations
        print "storing {} locations...".format(len(locations_to_save))
        HaltLocation.objects.bulk_create(locations_to_save)
