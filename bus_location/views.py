from django.db.models import Sum
from django.shortcuts import render

from bus_location.models import HaltLocation
from bus_location.helpers import mean
from bustracker.settings import GEOPOSITION_GOOGLE_MAPS_API_KEY


def fetch_top_locations(request):
    """Fetch most halted locations."""
    no_of_results = 20

    context = {}
    location_list = []  # will contain tuples in format: ((x, y), t)
    all_x = []
    all_y = []
    for halt_loc in HaltLocation.objects.values('location').annotate(
        total_duration=Sum('duration')
    ).order_by('-total_duration', 'location')[:no_of_results + 1]:
        location_list.append(((halt_loc['location'].x, halt_loc['location'].y), halt_loc['total_duration']))
        all_x.append(halt_loc['location'].x)
        all_y.append(halt_loc['location'].y)

    context['locations'] = location_list
    # a location around which the map will be centered by default
    context['center_location'] = (mean(all_x), mean(all_y))
    context['api_key'] = GEOPOSITION_GOOGLE_MAPS_API_KEY
    return render(request, 'bus_location/index.html', context)
