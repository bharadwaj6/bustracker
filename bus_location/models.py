"""Models for bus data and location data."""
from __future__ import unicode_literals

from django.contrib.gis.db import models


class BusLocation(models.Model):
    """Bus location data along with bus speed and ID."""

    bus_id = models.PositiveIntegerField(null=False)
    gps_timestamp = models.DateTimeField(null=False)
    location = models.PointField(null=False)
    speed = models.FloatField(null=False)

    class Meta:
        ordering = ['bus_id', 'gps_timestamp']


class HaltLocation(models.Model):
    """Locations along with bus_ids and how long they have been there."""

    location = models.PointField(null=False)
    duration = models.IntegerField(null=False)  # time in seconds
    bus_id = models.PositiveIntegerField()
