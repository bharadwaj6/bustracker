from django.conf.urls import url

from bus_location.views import fetch_top_locations

urlpatterns = [
    url(r'^', fetch_top_locations, name="index")
]
